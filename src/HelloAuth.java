import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class HelloAuth {

    private HttpClient client;
    
    public HelloAuth(HttpClient httpClient) {
        client = httpClient;
    }
    
	public static void main(String[] args) throws ClientProtocolException, URISyntaxException, IOException {
        new HelloAuth(HttpClientBuilder.create().build()).login();
	}

    public JSONObject login() throws URISyntaxException, IOException, ClientProtocolException {
        
		// Build request
        URI uri = new URIBuilder()
                .setScheme("http")
                .setHost("cavereg.baerbak.com")
                .setPort(4567)
                .setPath("/api/v1/auth")
                .setParameter("loginName", "rwar400t")
                .setParameter("password", "727b9cv")
                .build();
        HttpGet request = new HttpGet(uri);
        String requestStr = request.getRequestLine().getUri();
        System.out.println("Request: " + requestStr);

        // Execute request
        HttpResponse response = client.execute(request);

        // Get response
        HttpEntity httpEntity = response.getEntity();
        Reader buf = new InputStreamReader(httpEntity.getContent());
        JSONObject json = (JSONObject)JSONValue.parse(buf);
        System.out.println("JSONObject.toString(): " + json);
        
        return json;
    }
}
