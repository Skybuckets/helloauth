import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestHelloAuth {

    @Mock
    private HttpClient mockHttpClient;
    @Mock
    private HttpGet mockHttpPost;
    @Mock
    private HttpResponse mockHttpResponse;
    @Mock
    private HttpEntity mockHttpEntity; 
    @Mock
    private InputStream mockInputStream;
    
    @Before
    public void setup() {
        mockHttpClient = Mockito.mock(HttpClient.class);
        mockHttpPost = Mockito.mock(HttpGet.class);
        mockHttpResponse = Mockito.mock(HttpResponse.class);
        mockHttpEntity = Mockito.mock(HttpEntity.class);
    }

    @Test
    public void testLoginSuccess() throws ClientProtocolException, URISyntaxException, IOException {
        // Prepare mockups
        try {
            mockInputStream = new ByteArrayInputStream("{\"success\":true,\"subscription\":{\"groupName\":\"RWA4\",\"dateCreated\":\"2015-09-12 08:25 AM UTC\",\"playerName\":\"test-user-400\",\"loginName\":\"rwar400t\",\"region\":\"AARHUS\",\"playerID\":\"test-user-400\"},\"message\":\"loginName rwar400t was authenticated\"}".getBytes());
            Mockito.when(mockHttpClient.execute(any(HttpGet.class))).thenReturn(mockHttpResponse);
            Mockito.when(mockHttpResponse.getEntity()).thenReturn(mockHttpEntity);
            Mockito.when(mockHttpEntity.getContent()).thenReturn(mockInputStream);
        } catch (UnsupportedOperationException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }

        // Test method
        JSONObject json = new HelloAuth(mockHttpClient).login();
        assertEquals(Boolean.TRUE, json.get("success"));
    }

    @Test
    public void testLoginFail() throws ClientProtocolException, URISyntaxException, IOException {
        // Prepare mockups
        try {
            mockInputStream = new ByteArrayInputStream("{\"success\":false,\"message\":\"loginName rwar400tk was not authenticated\"}".getBytes());
            Mockito.when(mockHttpClient.execute(any(HttpGet.class))).thenReturn(mockHttpResponse);
            Mockito.when(mockHttpResponse.getEntity()).thenReturn(mockHttpEntity);
            Mockito.when(mockHttpEntity.getContent()).thenReturn(mockInputStream);
        } catch (UnsupportedOperationException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }

        // Test method
        JSONObject json = new HelloAuth(mockHttpClient).login();
        assertEquals(Boolean.FALSE, json.get("success"));
    }

    @SuppressWarnings("unchecked")
    @Test(expected=IOException.class)
    public void testLogin_IOException() throws ClientProtocolException, URISyntaxException, IOException {
        // Prepare mockups
        try {
            mockInputStream = new ByteArrayInputStream("{\"success\":false,\"message\":\"loginName rwar400tk was not authenticated\"}".getBytes());
            Mockito.when(mockHttpClient.execute(any(HttpGet.class))).thenThrow(IOException.class);
        } catch (UnsupportedOperationException e) {
            fail(e.getMessage());
        } catch (IOException e) {
            fail(e.getMessage());
        }

        // Test method (expect method to throw exception
        new HelloAuth(mockHttpClient).login();
    }
}
